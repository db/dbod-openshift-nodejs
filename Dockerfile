
FROM centos/s2i-base-centos7

# This image provides a Node.JS environment you can use to run your Node.JS
# applications.

MAINTAINER Jose Andres Cordero Benitez <j.cordero@cern.ch>

EXPOSE 8080

# Add $HOME/node_modules/.bin to the $PATH, allowing user to make npm scripts
# available on the CLI without using npm's --global installation mode
# This image will be initialized with "npm run $NPM_RUN"
# See https://docs.npmjs.com/misc/scripts, and your repo's package.json
# file for possible values of NPM_RUN
ENV NODEJS_VERSION=6 \
    NPM_RUN=start \
    NPM_CONFIG_PREFIX=$HOME/.npm-global \
    PATH=$HOME/node_modules/.bin/:$HOME/.npm-global/bin/:$PATH

ENV SUMMARY="Platform for building and running Node.js $NODEJS_VERSION applications" \
    DESCRIPTION="Node.js $NODEJS_VERSION available as docker container is a base platform for \
building and running various Node.js $NODEJS_VERSION applications and frameworks. \
Node.js is a platform built on Chrome's JavaScript runtime for easily building \
fast, scalable network applications. Node.js uses an event-driven, non-blocking I/O model \
that makes it lightweight and efficient, perfect for data-intensive real-time applications \
that run across distributed devices."

LABEL io.k8s.description="$DESCRIPTION" \
      io.k8s.display-name="Node.js" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="nodejs" \
      io.openshift.s2i.destination="/opt/s2i/destination" \
      com.redhat.deployments-dir="/opt/app-root/src"

RUN yum -y install epel-release && \
    yum -y install nodejs && \
    yum -y install npm && \
    mkdir -p /opt/app-root && \
    mkdir -p /opt/s2i/destination

RUN npm install npm@latest -g
RUN npm install -g n

# Copy the S2I scripts from the specific language image to $STI_SCRIPTS_PATH
RUN chmod +x ./s2i/bin/assemble ./s2i/bin/run ./s2i/bin/usage
COPY ./s2i/bin/ $STI_SCRIPTS_PATH

# Drop the root user and make the content of /opt/app-root owned by user 1001
RUN chown -R 1001:0 /opt/app-root && chmod -R ug+rwx /opt/app-root && \
    chown -R 1001:0 $STI_SCRIPTS_PATH && chmod -R ug+rwx $STI_SCRIPTS_PATH && \
    chmod -R g+rw /opt/s2i/destination

RUN bash -c 'echo -e node -v'
RUN bash -c 'echo -e npm -v'

USER 1001

# Set the default CMD to print the usage of the language image
CMD $STI_SCRIPTS_PATH/usage